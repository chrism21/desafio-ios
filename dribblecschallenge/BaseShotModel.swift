//
//  ShotBaseModel.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/8/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import Foundation
import JSONHelper

class BaseShotModel: Deserializable {
    var ID = 0
    var createdAt = ""
    var commentsCount = 0
    var pageURL = NSURL()
    var reboundsCount = 0
    var likesCount = 0
    
    init() {
        
    }
    
    required init(data: JSONDictionary) {
        ID <-- data["id"]
        createdAt <-- data["created_at"]
        commentsCount <-- data["comments_count"]
        pageURL <-- data["url"]
        reboundsCount <-- data["rebounds_count"]
        likesCount <-- data["likes_count"]
    }
}