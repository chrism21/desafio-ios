//
//  ShotDetailViewController.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/9/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import UIKit
import SDWebImage
import QuartzCore

class ShotDetailViewController: UIViewController, ViewControllerState, UIWebViewDelegate {
    var shotID: String!
    var shot: Shot?
    
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var shotDescription: UIWebView!
    
    @IBOutlet weak var shotTitle: UILabel!
    @IBOutlet weak var viewCount: UILabel!
    @IBOutlet weak var contentContainer: UIView!
    
    @IBOutlet weak var webViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var shotHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchShot()
    }
    
    func fetchShot() {
        let req = ShotDetailsViewControllerDataRequester(errorHandler: DefaultErrorHandler(), viewControllerStateDelegate: self, shotID: shotID)
        req.makeRequest { responseObject in
            self.shot = responseObject
            self.populateViews()
        }
    }
    
    func populateViews() {
        if let shot = self.shot {
            self.navigationItem.title = shot.title
            activityIndicator.startAnimating()
            mainImageView.sd_setImageWithURL(shot.imageZoomURL, placeholderImage: nil) { image, error, cacheType, url in
                self.activityIndicator.stopAnimating()
            }
            userImage.sd_setImageWithURL(shot.player.avatarURL)
            userImage.layer.masksToBounds = true
            userImage.layer.cornerRadius = userImage.frame.size.width / 2
            userName.text = shot.player.name
            shotDescription.loadHTMLString(shot.description, baseURL: nil)
            shotTitle.text = shot.title
            viewCount.text = String(shot.viewsCount)
            updateShotImageViewConstraints(UIApplication.sharedApplication().statusBarOrientation, isRotating: false)
        }
    }
    
    func updateShotImageViewConstraints(toOrientation: UIInterfaceOrientation, isRotating: Bool) {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return
        }
        
        if let shot = self.shot {
            let shouldUseShotHeight = toOrientation == .Portrait || toOrientation == .PortraitUpsideDown
            let bounds = UIApplication.sharedApplication().keyWindow?.rootViewController!.view.bounds
            let screenHeight = isRotating ? bounds!.width : (shouldUseShotHeight ? bounds!.width : bounds!.height)
            let screenWidth = isRotating ? bounds!.height : (shouldUseShotHeight ? bounds!.width : bounds!.height)
            let maxHeight = CGFloat(max(shot.height, 600))
            let shotWidthToScreenWidthRatio = maxHeight / screenWidth
            let ratio = shouldUseShotHeight ? CGFloat(1) : shotWidthToScreenWidthRatio
            self.shotHeightContraint.constant = maxHeight * ratio
            self.webViewHeightConstraint.constant = screenHeight - (userImage.bounds.size.height + self.shotHeightContraint.constant)
        }
    }
    
    // MARK:- ViewControllerState
    
    func startLoadingState() {
        adjustAlpha(0)
    }
    
    func finishLoadingState() {
        UIView.animateWithDuration(1, delay: 0, usingSpringWithDamping: 7, initialSpringVelocity: 7, options: .TransitionNone, animations: {
            self.adjustAlpha(1)
        }, completion: nil)
    }
    
    func adjustAlpha(alpha: CGFloat) {
        userImage.alpha = alpha
        userName.alpha = alpha
        shotDescription.alpha = alpha
        shotTitle.alpha = alpha
        viewCount.alpha = alpha
        mainImageView.alpha = alpha
        contentContainer.alpha = alpha > 0 ? 0.85 : alpha
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        let toOrientation = UIApplication.sharedApplication().isInLandscape() ? UIInterfaceOrientation.Portrait : UIInterfaceOrientation.LandscapeLeft
        updateShotImageViewConstraints(toOrientation, isRotating: true)
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        super.willRotateToInterfaceOrientation(toInterfaceOrientation, duration: duration)
        updateShotImageViewConstraints(toInterfaceOrientation, isRotating: true)
    }
    
    // MARK:- UIWebViewDelegate
    
    var shouldLoadWebViewContent = true
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return shouldLoadWebViewContent
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        shouldLoadWebViewContent = false
    }
}
