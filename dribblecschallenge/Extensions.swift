//
//  Extensions.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/9/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import UIKit

extension UIApplication {
    func isInLandscape() -> Bool {
        let currentOrientation = UIApplication.sharedApplication().statusBarOrientation
        return currentOrientation == .LandscapeLeft || currentOrientation == .LandscapeRight
    }
}

extension UIDevice {
    class func isPad() -> Bool {
        return UIDevice.currentDevice().userInterfaceIdiom == .Pad
    }
}

extension String {
    func numberOfOccurences(char: Character) -> Int {
        var numOccurences = 0
        for c in Array(self) {
            if c == char {
                numOccurences++
            }
        }
        return numOccurences
    }
}