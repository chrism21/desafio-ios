//
//  BaseAggregateDataRequester.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/8/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import UIKit

class BaseAggregateDataRequester {
    var errorHandler: ErrorHandler
    var viewControllerStateDelegate: ViewControllerState?
    var isLoading = false
    
    init() {
        self.errorHandler = DefaultErrorHandler()
    }
    
    init(errorHandler: ErrorHandler, viewControllerStateDelegate: ViewControllerState) {
        self.errorHandler = errorHandler
        self.viewControllerStateDelegate = viewControllerStateDelegate
    }
}