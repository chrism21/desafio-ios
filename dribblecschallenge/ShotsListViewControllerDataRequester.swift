//
//  ShotsListViewControllerDataRequester.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/8/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import Foundation

class ShotsListViewControllerDataRequester: BaseAggregateDataRequester, AggregateDataFetcher {
    
    var page: Int
    
    init(errorHandler: ErrorHandler, viewControllerStateDelegate: ViewControllerState, page: Int) {
        self.page = page
        super.init(errorHandler: errorHandler, viewControllerStateDelegate: viewControllerStateDelegate)
    }
    
    typealias ResponseObject = [Shot]
    
    func makeRequest(completion: (responseObject: ResponseObject) -> ()) {
        viewControllerStateDelegate?.startLoadingState()
        self.isLoading = true
        let req = ShotsRequest(page: page)
        req.makeRequest { result in
            self.isLoading = false
            switch result {
            case let .Success(shots):
                completion(responseObject: shots.value)
            case let .Failure(error):
                self.errorHandler.handleError(error)
            }
            self.viewControllerStateDelegate?.finishLoadingState()
        }
    }
}