//
//  ShotListCollectionViewCell.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/9/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import UIKit
import SDWebImage

class ShotListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var viewCount: UILabel!
    @IBOutlet weak var shotImage: UIImageView!
    
    func fillCell(#shot: Shot) {
        self.name.text = shot.title
        self.viewCount.text = String(shot.viewsCount)
        self.shotImage.sd_setImageWithURL(shot.imageDetailsURL ?? shot.imageCatalogURL)
    }
    
    class func size(collectionViewSize: CGSize, shotSize: CGSize) -> CGSize {
        let collectionDivided = collectionViewSize.width / divider()
        let width = shotSize.width * collectionDivided / shotSize.width
        let height = shotSize.height * collectionDivided / shotSize.width
        return CGSize(width: width, height: height)
    }
    
    class func divider() -> CGFloat {
        if UIDevice.isPad() {
            return UIApplication.sharedApplication().isInLandscape() ? 3.1 : 2.05
        }
        return UIApplication.sharedApplication().isInLandscape() ? 2.05 : 1
    }
}