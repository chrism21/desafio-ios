//
//  ShotDetailsRequest.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/9/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import Foundation

import JSONHelper

class ShotDetailsRequest: BaseRequest, Request {
    
    init(shotID: String) {
        super.init(requestPath: "shots/\(shotID)")
    }
    
    typealias ResultType = Shot
    
    func makeRequest(completion: (result: Result<ResultType>) -> ()) {
        self.get(url: self.requestPath, parameters: nil, success: { (object) -> () in
            completion(result: .Success(Box(Shot(data: object as JSONDictionary))))
            }) { (error) -> () in
            completion(result: .Failure(error))
        }
    }
}