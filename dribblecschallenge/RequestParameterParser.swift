//
//  RequestParameterParser.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/9/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import Foundation

public class RequestParameterParser {
    var parameters: [String: AnyObject]
    init(parameters: [String: AnyObject]) {
        self.parameters = parameters
    }
    
    func parse(#insertQuery: Bool) -> String {
        var parameterString = insertQuery ? "?" : ""
        for (key, value) in parameters {
            let ampersand = Array(parameterString).count > 1 ? "&" : ""
            parameterString =  parameterString + ampersand + key + "=" + "\(value)"
        }
        return parameterString
    }
}