//
//  ShotDetailsViewControllerDataRequester.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/9/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import Foundation

class ShotDetailsViewControllerDataRequester: BaseAggregateDataRequester, AggregateDataFetcher {
    
    var shotID: String
    
    init(errorHandler: ErrorHandler, viewControllerStateDelegate: ViewControllerState, shotID: String) {
        self.shotID = shotID
        super.init(errorHandler: errorHandler, viewControllerStateDelegate: viewControllerStateDelegate)
    }
    
    typealias ResponseObject = Shot
    
    func makeRequest(completion: (responseObject: ResponseObject) -> ()) {
        viewControllerStateDelegate?.startLoadingState()
        self.isLoading = true
        let req = ShotDetailsRequest(shotID: shotID)
        req.makeRequest { result in
            self.isLoading = false
            switch result {
            case let .Success(shots):
                completion(responseObject: shots.value)
            case let .Failure(error):
                self.errorHandler.handleError(error)
            }
            self.viewControllerStateDelegate?.finishLoadingState()
        }
    }
}