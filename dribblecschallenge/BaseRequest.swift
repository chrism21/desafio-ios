//
//  BaseRequest.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/8/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import Foundation
import Alamofire

let kBaseURL = "http://api.dribbble.com/"

class BaseRequest {
    typealias SuccessBlock = (object: AnyObject) -> ()
    typealias FailureBlock = (error: NSError) -> ()
    typealias Parameters = [String: AnyObject]
    
    var requestPath: String
    
    init(requestPath: String) {
        self.requestPath = requestPath
    }
    
    init() {
        self.requestPath = ""
    }
    
    func get(#url: String, parameters: Parameters?, success: SuccessBlock, failure: FailureBlock) {
        var finalURL = url
        if let unwrappedParameters = parameters {
            finalURL = url + RequestParameterParser(parameters: unwrappedParameters).parse(insertQuery: true)
        }
        Alamofire.request(.GET, kBaseURL + finalURL).responseJSON { (_, _, JSON, error) in
            error == nil ? success(object: JSON!) : failure(error: error!)
        }
    }
}