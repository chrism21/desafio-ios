//
//  Player.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/8/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import Foundation
import JSONHelper

class Player: BaseShotModel, Deserializable {
    
    var avatarURL = NSURL()
    var commentsReceivedCount = 0
    var draftedByPlayerID = ""
    var drafteesCount = 0
    var followersCount = 0
    var followingCount = 0
    var likesReceivedCount = 0
    var location = ""
    var name = ""
    var reboundsReceivedCount = 0
    var shotsCount = 0
    var twitterScreenName = ""
    var username = ""
    var websiteURL = NSURL()
    
    override init() {
        super.init()
    }
    
    required init(data: JSONDictionary) {
        super.init(data: data)
        avatarURL <-- data["avatar_url"]
        commentsReceivedCount <-- data["comments_received_count"]
        draftedByPlayerID <-- data["drafted_by_player_id"]
        drafteesCount <-- data["draftees_count"]
        followersCount <-- data["followers_count"]
        followingCount <-- data["following_count"]
        likesReceivedCount <-- data["likes_received_count"]
        location <-- data["location"]
        name <-- data["name"]
        reboundsReceivedCount <-- data["rebounds_received_count"]
        twitterScreenName <-- data["twitter_screen_name"]
        username <-- data["username"]
        websiteURL <-- data["website_url"]
    }
}
