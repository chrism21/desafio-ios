//
//  ErrorHandler.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/8/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import Foundation

protocol ErrorHandler {
    func handleError(error: NSError)
}
