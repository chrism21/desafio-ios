//
//  ShotsRequest.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/8/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import Foundation
import JSONHelper

class ShotsRequest: BaseRequest, Request {
    
    var page: Int
    
    init(page: Int) {
        self.page = page
        super.init(requestPath: "shots/popular/")
    }
    
    typealias ResultType = [Shot]
    
    func makeRequest(completion: (result: Result<ResultType>) -> ()) {
        self.get(url: self.requestPath, parameters: ["page": String(self.page)], success: { (object) -> () in
            let json = object as JSONDictionary
            var shots: [Shot]?
            shots <-- json["shots"]
            if let unwrappedShots = shots {
                completion(result: .Success(Box(unwrappedShots)))
            } else {
                completion(result: .Failure(NSError(domain: "Shots", code: 0, userInfo: nil)))
            }
        }) { (error) -> () in
            completion(result: .Failure(error))
        }
    }
}