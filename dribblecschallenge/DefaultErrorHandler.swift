//
//  DefaultErrorHandler.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/8/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import UIKit

class DefaultErrorHandler: ErrorHandler {
    func handleError(error: NSError) {
        let alert = UIAlertView(title: nil, message: "Default message because the developer was too lazy to figure one out :)", delegate: nil, cancelButtonTitle: "Cancel")
        alert.show()
    }
}