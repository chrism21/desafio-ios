//
//  ShotListViewController.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/8/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import UIKit

class ShotListViewController: UIViewController, ViewControllerState, UICollectionViewDataSource, UICollectionViewDelegate {
    var page = 1
    var shots = [Shot]()
    var request: ShotsListViewControllerDataRequester!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchShots()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView.performBatchUpdates({
            self.collectionView.reloadData()
            }, completion: nil)
    }
    
    func fetchShots() {
        self.request = ShotsListViewControllerDataRequester(errorHandler: DefaultErrorHandler(), viewControllerStateDelegate: self, page: page)
        request.makeRequest { shots in
            self.page++
            self.shots += shots
        }
    }
    
    // MARK:- ViewControllerState
    
    func startLoadingState() {
        self.collectionView.hidden = self.shots.count == 0
    }
    
    func finishLoadingState() {
        self.collectionView.hidden = false
        self.collectionView.reloadData()
    }
    
    // MARK:- CollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.shots.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let shot = self.shots[indexPath.row]
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ShotList", forIndexPath: indexPath) as ShotListCollectionViewCell
        cell.fillCell(shot: shot)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let shot = self.shots[indexPath.row]
        return ShotListCollectionViewCell.size(collectionView.bounds.size, shotSize: CGSize(width: shot.width, height: shot.height))
    }
    
    // MARK:- CollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let shot = self.shots[indexPath.row]
        let detailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ShotDetailViewController") as ShotDetailViewController
        detailsViewController.shotID = String(shot.ID)
        self.navigationController?.pushViewController(detailsViewController, animated: true)
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        self.collectionView.performBatchUpdates({
            self.collectionView.reloadData()
        }, completion: nil)
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        self.collectionView.performBatchUpdates({
            self.collectionView.reloadData()
            }, completion: nil)
    }
    
    // MARK:- Instance methods
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if self.scrollDidSurpassReloadThreshold(scrollView) && !self.request.isLoading {
            self.fetchShots()
        }
    }
    
    func scrollDidSurpassReloadThreshold(scrollView: UIScrollView) -> Bool {
        let height = scrollView.contentSize.height
        let y = scrollView.contentOffset.y
        let totalItems = CGFloat(self.shots.count)
        
        let yConverted = y / totalItems
        let heightConverted = height / totalItems
        let itemsSeen = (yConverted / heightConverted) * totalItems
        
        let thresholdPadding = CGFloat(UIDevice.currentDevice().userInterfaceIdiom == .Pad ? 3.0 : 1.0)
        let threshold = totalItems - (CGFloat(self.shots.count / self.page) * thresholdPadding)
        return itemsSeen > threshold;
    }
}

