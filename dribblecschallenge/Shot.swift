//
//  Shot.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/8/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import Foundation
import JSONHelper

class Shot: BaseShotModel, Deserializable {
    
    var description = ""
    var height = 0
    var width = 0
    var imageCatalogURL = NSURL()
    var imageDetailsURL = NSURL()
    var imageZoomURL = NSURL()
    var reboundSourceID = ""
    var shortURL = NSURL()
    var title = ""
    var viewsCount = 0
    
    var player = Player()
    
    required init(data: JSONDictionary) {
        super.init(data: data)
        description <-- data["description"]
        height <-- data["height"]
        width <-- data["width"]
        
        imageCatalogURL <-- data["image_teaser_url"]
        imageDetailsURL <-- data["image_400_url"]
        imageZoomURL <-- data["image_url"]
        
        reboundSourceID <-- data["rebound_source_id"]
        shortURL <-- data["short_url"]
        title <-- data["title"]
        
        viewsCount <-- data["views_count"]
        
        player <-- data["player"]
    }
}