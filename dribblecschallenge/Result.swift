//
//  Result.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/8/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import Foundation

final class Box<T> {
    let value: T
    
    init(_ value: T) {
        self.value = value
    }
}

enum Result<A> {
    case Failure(NSError)
    case Success(Box<A>)
}