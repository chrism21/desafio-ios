//
//  RequestParameterParserTest.swift
//  dribblecschallenge
//
//  Created by Chris Morris on 4/9/15.
//  Copyright (c) 2015 Chris Morris. All rights reserved.
//

import XCTest

class RequestParameterParserTest: XCTestCase {
    
    let stubParameters = ["cs": "challenge", "was": "fun", "thanks": "for", "the": "opportunity"]

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testShouldInitializeParser() {
        let parser = RequestParameterParser(parameters: stubParameters)
        let actualParameters = parser.parameters
        XCTAssertEqual(actualParameters["cs"] as String, "challenge")
        XCTAssertEqual(actualParameters["was"] as String, "fun")
        XCTAssertEqual(actualParameters["thanks"] as String, "for")
        XCTAssertEqual(actualParameters["the"] as String, "opportunity")
    }
    
    /// Dictionaries are non-ordered two-tuples data structures
    /// thus the parameter strings cannot be safely compared
    /// directly using the Equatable protocol
    func testShouldCreateParametersWithoutQuery() {
        let parser = RequestParameterParser(parameters: stubParameters)
        let expectedParameters = "cs=challenge&was=fun&thanks=for&the=opportunity"
        let actualParameters = parser.parse(insertQuery: false)
        let numExpectedAmpersand = 3
        let numActualAmpersand = actualParameters.numberOfOccurences("&")
        XCTAssertEqual(numExpectedAmpersand, numActualAmpersand)
        XCTAssertEqual(Array(expectedParameters).count, Array(actualParameters).count)
    }
    
    func testShouldCreateParametersWithQuery() {
        let parser = RequestParameterParser(parameters: stubParameters)
        let expectedParameters = "?cs=challenge&was=fun&thanks=for&the=opportunity"
        let actualParameters = parser.parse(insertQuery: true)
        let numExpectedAmpersand = 3
        let numActualAmpersand = actualParameters.numberOfOccurences("&")
        let expectedFirstChar = "?"
        let actualFirstChar = actualParameters.substringToIndex(advance(actualParameters.startIndex, 1))
        XCTAssertEqual(numExpectedAmpersand, numActualAmpersand)
        XCTAssertEqual(Array(expectedParameters).count, Array(actualParameters).count)
        XCTAssertEqual(expectedFirstChar, actualFirstChar)
    }
}










